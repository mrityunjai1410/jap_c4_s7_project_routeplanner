package com.routeplanner.standard;

import com.routeplanner.entity.Route;

import java.io.IOException;

public interface IRouteDao {

    void fetchDataFromCsv() throws IOException;

    Route[] findall();
}
