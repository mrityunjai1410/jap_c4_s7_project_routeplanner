package com.routeplanner.entity;

public class Route {

    private String from;
    private String to;
    private String distance;
    private String travelTime;
    private String airfare;

    public Route(){

    }

    public Route(String from,String to,String distance,String travelTime,String airfare){
        this.from = from;
        this.to = to;
        this.airfare = airfare;
        this.distance =distance;
        this.travelTime = travelTime;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAirfare() {
        return airfare;
    }

    public void setAirfare(String airfare) {
        this.airfare = airfare;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

}
