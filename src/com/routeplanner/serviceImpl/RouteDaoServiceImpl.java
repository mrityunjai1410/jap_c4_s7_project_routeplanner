package com.routeplanner.serviceImpl;

import com.routeplanner.entity.Route;
import com.routeplanner.standard.IRouteDao;

import java.io.*;

public class RouteDaoServiceImpl implements IRouteDao {
    private Route[] routes = new Route[5];

    @Override
    public void fetchDataFromCsv() throws IOException {
        File file = new File("D:\\FSD\\sprint2\\JAP_C4_S7_Project_RoutePlanner\\src\\com\\routeplanner\\csvfile\\input.txt");
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line = "";
        int index = 0;
        while((line = bufferedReader.readLine())!=null){
            String[] splitline = line.split(",");
            routes[index] = new Route(splitline[0],splitline[1],splitline[2],splitline[3],splitline[4]);
            index++;
        }

    }

    @Override
    public Route[] findall() {
        return routes;
    }

    public Route[] getRoutes() {
        return routes;
    }

}
